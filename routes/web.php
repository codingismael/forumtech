<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/forum', 'ForumController@index')->name('forum');

Route::get('{provider}/auth','SocialController@auth')->name('social.auth');

Route::get('{provider}/redirect','SocialController@auth_callback')->name('social.authcallback');

Route::resource('channels','ChannelController')->middleware('auth');

Route::resource('discussions','DiscussionController')->middleware('auth');

Route::post('/discussions/reply/{id}','DiscussionController@reply')->middleware('auth');



Route::get('/likes/like/{id}','LikeController@like')->middleware('auth');

Route::get('/likes/unlike/{id}','LikeController@unlike')->middleware('auth');

Route::get('/channel/{slug}','ForumController@channel')->middleware('auth');

Route::get('/watch/{id}','WatcherController@watch')->middleware('auth');

Route::get('/unwatch/{id}','WatcherController@unwatch')->middleware('auth');

Route::get('/reply/bestanswer/{id}','DiscussionController@markbestanswer')->middleware('auth');

