<?php

use Illuminate\Database\Seeder;

class ChannelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Channel::create([
          
          'title'=>'java',
          'slug'=>'java',
        ]);


         App\Channel::create([
          
          'title'=>'crypto',
          'slug'=>'crypto',
        ]);


          App\Channel::create([
          
          'title'=>'html',
          'slug'=>'html',
        ]);
    }
}
