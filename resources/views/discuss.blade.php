@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
          @if($errors->any())
            <div class="alert alert-danger" role="alert">
                <ul>
                    
                    @foreach($errors->all() as $error)
                        <li> {{$error}}</li>
                    @endforeach

                </ul>
              
            </div>
            @endif
            <div class="card">
                <div class="card-header">Create A Discussion</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="{{route('discussions.store')}}" method="post">
                        @csrf
                        
                      <div class="form-group">
                          
                            <select name="channel_id" id="channel_id">
                              
                                  @foreach($channels as $channel)
                                  <option value="{{$channel->id}}">{{$channel->title}}</option>
                                  @endforeach
                            </select>

                      </div>

                      <div class="form-group">
                        
                        <input type="text" name ="title" placeholder="Entrer un titre">

                      </div>


                      <div class="form-group">
                        
                        <textarea name="content" id="content" cols="50" rows="10">ask a question</textarea>

                      </div>

                      <div class="form-group">
                          

                          <button class="btn-success" type='submit'>Add A DISCUSSION</button>
                      </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection