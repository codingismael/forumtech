@extends('layouts.app')

@section('content')

            <div class="card">
                <div class="card-header"><img src="{{$thediscussion->user->avatar}}" height="40px" width="40px" style="border-radius:  50%;" alt=""> {{$thediscussion->user->name}} ({{$thediscussion->user->experience}} Pts) 

                
                 @if($thediscussion->hasbestanswer() != 1)
                              @if($thediscussion->watchedOrNot())
                            <a href="/unwatch/{{$thediscussion->id}}" class="btn btn-sm btn-primary" style="float:right;">unwatch</a>
                        @else
                            <a href="/watch/{{$thediscussion->id}}" class="btn btn-sm btn-primary" style="float:right;">watch</a>
                        @endif
                     <button class="btn btn-outline-success btn-sm" style="float:right;">Open</button>
                 @else
                     <button class="btn btn-outline-danger btn-sm" style="float:right;">Closed</button> 
                 @endif
                </div>

                <div class="card-body">

                    <h4 class="text-center">{{$thediscussion->title}}</h4> 

                     <p class="text-center">{{$thediscussion->content}}</p>   



    
                </div>
            </div><br>

             <?php $pp = $thediscussion->replies()->paginate(1);
                    $bestans = 0;

                    if($thediscussion->hasbestanswer()){$bestans = 1 ;} 
              ?>

              @if ($bestans) 
                                                <h4 class="text-center">Best Answer</h4>
                <div class="card card-blue" style = "box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);">
                    <div class="card-header"><img src="{{$bestansy->user->avatar}}" height="40px" width="40px" alt="" style="border-radius:  50%;"> {{$bestansy->user->name}} ({{$bestansy->user->experience}} Pts)   </div>

                    <div class="card-body">

                    {{$bestansy->content}}


                </div>

                <div class="card-footer">@if($bestansy->is_already_liked()) <a href="/likes/unlike/{{$bestansy->id}}" class="btn btn-info btn-xs">Liked</a>  @else <a href="/likes/like/{{$bestansy->id}}" class="btn btn-danger btn-xs">Like</a> @endif
                 {{$bestansy->likes->count()}} Likes
                 </div>
                </div>

            @endif
            <br>

              
            @foreach($pp as $onereply)
            <div class="card">
                <div class="card-header"><img src="{{$onereply->user->avatar}}" height="40px" width="40px" alt="" style="border-radius:  50%;"> {{$onereply->user->name}} ({{$onereply->user->experience}} Pts)   
                @if( ! $bestans)
                @if($thediscussion->user->id == Auth::id())
                 <a href="/reply/bestanswer/{{$onereply->id}}" class="btn btn-sm btn-info" style="float:right;">Mark best Answer</a> 
                 
                 @endif


             @endif </div>

                <div class="card-body">

                    {{$onereply->content}}


                </div>
                <div class="card-footer">@if($onereply->is_already_liked()) <a href="/likes/unlike/{{$onereply->id}}" class="btn btn-info btn-xs">Liked</a>  @else <a href="/likes/like/{{$onereply->id}}" class="btn btn-danger btn-xs">Like</a> @endif
                 {{$onereply->likes->count()}} Likes
                 </div>
            </div><br>
            @endforeach

            {{$pp->links()}}

            @if($thediscussion->hasbestanswer() != 1)
            
            @if($errors->any())
            <div class="alert alert-danger" role="alert">
                <ul>
                    
                    @foreach($errors->all() as $error)
                        <li> {{$error}}</li>
                    @endforeach

                </ul>
              
            </div>
            @endif

            <div class="card">

                <div class="card-header">
                  <h4>Reply</h4>  

                </div>
                
                <div class="card-body">
                    
                        <form action="/discussions/reply/{{$thediscussion->id}}" method="post">
                            @csrf
                            
                            <div class="form-group">
                                
                                
                                <textarea name="reply" id="reply" cols="100" rows="10">
                                    

                                </textarea>
                            </div>

                            <div class="form-group">
                                

                                <button class="btn btn-info" type="submit">submit</button>
                            </div>

                        </form>

                </div>


            </div>

            @endif

@endsection
