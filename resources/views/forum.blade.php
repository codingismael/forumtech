@extends('layouts.app')

@section('content')

            @foreach($thediscussions as $onediscussion)
            <div class="card">
                <div class="card-header">

                    <img src="{{$onediscussion->user->avatar}}" alt="" height="40px" width="40px" >&nbsp;&nbsp;&nbsp;
                    <span>{{$onediscussion->user->name}}&nbsp;&nbsp;&nbsp;<b>created: {{$onediscussion->created_at->diffForHumans()}}</b></span>
                    @if($onediscussion->hasbestanswer() != 1)  <button type="button" class="btn btn-outline-success btn-sm" style="float: right;">Open</button> @else <button class="btn btn-outline-danger btn-sm" style="float:right;">Closed</button>  @endif  
                    <a href="discussions/{{$onediscussion->slug}}" class ="btn btn-info btn-sm" style="float: right;">view</a>

                </div>

                <div class="card-body">
                <h4 class="text-center">{{$onediscussion->title}}</h4>
                  <p class="text-center">{{str_limit($onediscussion->content,100)}}</p>  

                </div>
                <div class="card-footer"> <?php $a = $onediscussion->replies->count() ; ?>  @if($a==1){{$a}} reply @else {{$a}} replies @endif  <a href="/channel/{{$onediscussion->channel->slug}}" class="btn btn-warning btn-sm" style="float:right;"> {{$onediscussion->channel->title}}</a></div>
            </div><br>@endforeach
            <div class="text-center">{{$thediscussions->links()}}</div>
            

@endsection
