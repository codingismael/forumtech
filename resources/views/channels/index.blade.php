@extends('layouts.app')

@section('content')

            <div class="card">
                <div class="card-header">Channels</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-striped">
                        <thead>
                        <tr><th> Channel</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr></thead>
                    @if(!$channels->isEmpty())
                    @foreach($channels as $channel)
                    <tr>
                        
                        <td>{{$channel->title}} </td>
                        <td><a href="{{route('channels.edit',['channel'=>$channel->id])}}" class="btn btn-info">Edit</a> </td>
                        <td><form action="{{route('channels.destroy',['channel'=>$channel->id])}}" method="post">
                            @method('DELETE')
                            @csrf
                            <button class="btn btn-xs btn-danger" type="submit"> Delete</button>

                        </form>

                             </td>
                        
                    </tr>

                     @endforeach
                     @endif   
                    </table>
                </div>
            </div>

@endsection
