@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create A Channel</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="{{route('channels.store')}}" method="post">
                        @csrf
                        
                      <div class="form-group">
                          
                            <input type="text" name="title" class="form-control">

                      </div>

                      <div class="form-group">
                          

                          <button class="btn-success" type='submit'>Add channel</button>
                      </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
