<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Discussion extends Model
{
    public function channel(){

     return	$this->belongsTo('App\Channel');
    }

    public function user(){

    	return $this->belongsTo('App\User');
    }
    public function replies(){

    	return $this->hasMany('App\Reply');
    }
     public function watchers(){

     	return $this->hasMany('App\Watcher');
     }

     public function watchedOrNot(){

     		return $this->watchers()->where('user_id',Auth::id())->get()->isNotEmpty();
     }

     public function hasbestanswer(){


        return $this->replies()->where('best_answer',1)->get()->isNotEmpty();
     }
}
