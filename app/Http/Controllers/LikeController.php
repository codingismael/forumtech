<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Like;
use App\Reply;
use Auth;

class LikeController extends Controller
{
    
	public function like($id){

		

		$mylike = new Like;

		$mylike->user_id = Auth::id();
		$mylike->reply_id = $id;

		$mylike->save();

		return redirect()->back();


	}

	public function unlike($id){


		$mylike = Like::where('user_id',Auth::id())->where('reply_id',$id);

		$mylike->delete();

		return redirect()->back();

		
	}

}
