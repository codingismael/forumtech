<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Watcher;
use Auth;

class WatcherController extends Controller
{
    public function watch($id){


    	$newwatcher = new Watcher;

    	$newwatcher->user_id = Auth::id();

    	$newwatcher->discussion_id = $id;

    	$newwatcher->save();

    	return redirect()->back();

    }

    public function unwatch($id){


    	$rempli = Watcher::where('discussion_id',$id)->where('user_id',Auth::id())->first();

    	$rempli->delete();

    	return redirect()->back();
    }
}
