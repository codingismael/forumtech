<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Pagination\LengthAwarePaginator;
use App\Discussion;
use App\Channel;
use Auth;

class ForumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        switch(request('filter')){


            case 'me':
                $thediscussions = Discussion::where('user_id',Auth::id())->paginate(3);
                $thediscussions->withPath('forum?filter=me');
                break;

            case'solved':
            
            $arrayofsolved = array();

            $alldiscussions = Discussion::all();

            foreach($alldiscussions as $onediscussion){

                if($onediscussion->hasbestanswer()){

                    array_push($arrayofsolved,$onediscussion);
                }
            }

            $thediscussions = new LengthAwarePaginator($arrayofsolved,count($arrayofsolved),3);
            $thediscussions->withPath('forum?filter=solved');

       /*     probleme ca donne pas ce qu on veux en pagination*/
            break;


            default:
                $thediscussions = Discussion::paginate(3);

                break;

        }

        

        return view('forum',['thediscussions'=>$thediscussions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function channel($slug){

        $thechan = Channel::where('slug',$slug)->first();

        return view('channel')->with('discussions',$thechan->discussions()->paginate(1))->with('namos',$thechan->title);
    }


   
}
