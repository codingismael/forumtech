<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Channel;
use Session;

class ChannelController extends Controller
{



    public function __construct(){

            $this->middleware('admin');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$channels = DB::table('channels')->get();*/

        /*dd($channels);*/

        /*on les a envoyer golbalement*/



        return view('channels.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('channels.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([

                'title'=>'required'

        ]);
        
        $newchannel = new Channel;
        $newchannel->title = $request->title;
        $newchannel->slug = str_slug($request->title);

        $newchannel->save();

        Session::flash('success','new channel created');

        return redirect('channels');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $channels= DB::table('channels')->where('id','=',$id)->first();

        

        return view('channels.edit')->with('channel',$channels);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $channeltoedit = Channel::find($id);

        $channeltoedit->title = $request->title;
        $channeltoedit->slug = str_slug($request->title);
        $channeltoedit->save();

        return redirect('channels');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
       $channel = DB::table('channels')->where('id','=',$id);

       
        $channel->delete();

        return redirect()->back();

    }
}
