<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifications\NewReplyAdded;
use Notification;
use App\Discussion;
use App\Reply;
use App\Watcher;
use App\User;
use Auth;

class DiscussionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('discuss');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       

        $request->validate([

                'title'=>'required',
        
                'content'=>'required',




        ]);


        $newdiscuss = new Discussion;
        $newdiscuss->title = $request->title;
        $newdiscuss->slug = str_slug($request->title);
        $newdiscuss->content = $request->content;
        $newdiscuss->channel_id = $request->channel_id;
        $newdiscuss->user_id = Auth::id();
        $newdiscuss->user->experience += 50;

        $newdiscuss->user->save();

        $newdiscuss->save();

        return redirect('discussions/'.$newdiscuss->slug);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $thediscussion = Discussion::where('slug',$slug)->first();

        $bestansy = Reply::where('discussion_id',$thediscussion->id)->where('best_answer',1)->first();

        

        return view('discussions.show')->with('thediscussion',$thediscussion)->with('bestansy',$bestansy);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function reply($id){



            request()->validate([
                'reply'=>'required',
            ]);
           $discus = Discussion::find($id);

           $myreply = New Reply;

           $myreply->content = request()->reply;
           $myreply->user_id = Auth::id();

           $myreply->discussion_id = $id;

           $myreply->save();

           $myreply->user->experience += 30;
           $myreply->user->save();




           $towhom = $discus->watchers;

           foreach($towhom as $oneperson):

                $theuser = User::find($oneperson->user_id);
                Notification::send($theuser,new NewReplyAdded($discus));

            endforeach;

          /* Notification::send($towhom,new NewReplyAdded());*/

         /*  $watchers = array();
        foreach($discus->watchers as $watcher):
            array_push($watchers, User::find($watcher->user_id));
        endforeach;
        Notification::send($watchers, new \App\Notifications\NewReplyAdded());*/

           return redirect()->back();

    }



    public function markbestanswer($id){


            $bestreply = Reply::find($id);
            $ladiscus = Discussion::find($bestreply->discussion_id);
            if($ladiscus->user_id == Auth::id()){
            $bestreply->best_answer = 1 ;

            $bestreply->save();
            $ladiscus->user->experience += 50;
            $ladiscus->user->save();

/*            on veux pas que le createur de la discussion se donner des points a lui meme e n se tagant best reply.
            on lui a deja donner 50 juste pour le fait de tagger*/
            if ($ladiscus->user_id != $bestreply->user_id){

                    $bestreply->user->experience += 200;
                    $bestreply->user->save();
            }

            }


            return redirect()->back();


    }
}
