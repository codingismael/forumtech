<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Reply extends Model
{
    public function dicussion(){

    	return $this->belongsTo('App\Discussion');
    }

    public function user(){

    	return $this->belongsTo('App\User');
    }

    public function likes(){

    	return $this->hasMany('App\Like');
    }

   	 public function is_already_liked(){

    	$likedoupas = $this->likes()->where('user_id',Auth::id())->get()->isNotEmpty();

        
    	return $likedoupas;
    }

    
}
