<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
   public function user(){

   	$this->belongsTo('App\User');
   }

   public function discussions(){

   	return $this->hasMany('App\Discussion');
   }
}
